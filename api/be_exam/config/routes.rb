# frozen_string_literal: true

Rails.application.routes.draw do
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  namespace :api do
    post 'register', to: 'users#register'
    post 'login', to: 'users#login'
    post 'logout', to: 'users#logout'

    resources :posts do
      resources :comments
    end
  end
end
