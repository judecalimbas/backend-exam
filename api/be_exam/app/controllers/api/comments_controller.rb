# frozen_string_literal: true

class Api::CommentsController < ApplicationController
  before_action :set_resource, only: [:update, :destroy]

  def index
    @comments = post.comments

    render json: { data: @comments, links: {}, meta: {} }
  end

  def create
    @comment = post.comments.build(comment_params)

    if @comment.save
      render json: { data: @comment }
    else
      render_errors
    end
  end

  def update
    if @comment.update(comment_params)
      render json: { data: @comment }
    else
      render_errors
    end
  end

  def destroy
    if @comment.destroy
      render json: { status: I18n.t('record_deleted') }, status: :ok
    else
      render_errors
    end
  end

  private

  def set_resource
    @comment = Comment.find(params[:id])
  rescue ActiveRecord::RecordNotFound
    render json: { message: 'No query results for model Comment' }, status: :not_found
  end

  def render_errors
    render json: { message: I18n.t('invalid_data'), errors: @comment.errors }, status: :unprocessable_entity
  end

  def post
    @post ||= Post.find_by_id_or_slug(params[:post_id])
  end

  def comment_params
    params.permit(:body, :parent_id).merge(
      commentable_type: post.class.name,
      creator_id: current_user.id
    )
  end
end
