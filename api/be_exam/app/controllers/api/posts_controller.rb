class Api::PostsController < ApplicationController
  delegate :url_helpers, to: 'Rails.application.routes'
  delegate :helpers, to: 'ActionController::Base'

  before_action :set_resource, only: [:show, :destroy, :update]

  def index
    @posts = Post.page(params[:page])

    render json: { data: @posts }.merge(pagination)
  end

  def show
    render json: { data: @post }
  end

  def update
    if @post.update(post_params)
      render json: { data: @post }
    else
      render_errors
    end
  end

  def create
    @post = current_user.posts.build(post_params)

    if @post.save
      render json: { data: @post }
    else
      render_errors
    end
  end

  def destroy
    if @post.destroy
      render json: { message: I18n.t('record_deleted') }
    else
      render_errors
    end
  end

  private

  def set_resource
    @post = Post.find_by_id_or_slug(params[:id])
  rescue ActiveRecord::RecordNotFound
    render json: { message: 'No query results for model Post' }, status: :not_found
  end

  def render_errors
    render json: { message: I18n.t('invalid_data'), errors: @post.errors }, status: :unprocessable_entity
  end

  def post_params
    params.permit(:title, :content, :image)
  end

  def pagination
    {
      meta: {
        per_page: 25,
        current_page: @posts.current_page,
        from: 1,
        last_page: 1,
        path: url_helpers.api_posts_url,
        to: @posts.total_pages,
        total: @posts.size,
      },
      links: {
        first: helpers.path_to_prev_page(@posts),
        last: helpers.path_to_prev_page(@posts),
        prev: helpers.path_to_prev_page(@posts),
        next: helpers.path_to_next_page(@posts)
      },
    }
  end
end
