# frozen_string_literal: true

class Api::UsersController < ApplicationController
  def login
    @user = User.find_by(login_params) || User.new

    if @user.persisted?
      render json: @user.token_data
    else
      @user.erros.add(:email, 'field is required') if login_params[:email].blank?
      @user.erros.add(:password, 'field is required') if login_params[:password].blank?
      @user.errors.add(:email, 'These credentials do not match our records') if @user.errors.blank?

      render_errors
    end
  end

  def register
    @user = User.new(register_params)

    if @user.save
      render json: @user
    else
      render_errors
    end
  end

  def logout
    current_user.regenerate_token

    head :ok
  end

  private

  def render_errors
    render json: { message: I18n.t('invalid_data'), errors: @user.errors }, status: :unprocessable_entity
  end

  def register_params
    params.permit(:name, :email, :password, :password_confirmation)
  end

  def login_params
    params.permit(:email, :password)
  end
end
