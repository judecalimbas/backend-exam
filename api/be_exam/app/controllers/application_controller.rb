class ApplicationController < ActionController::API
  before_action :authenticate

  def authenticate
    current_user
  end

  def current_user
    @current_user ||= User.find_by(token: token)
  end

  def token
    pattern = /^Bearer /
    header  = request.env["HTTP_AUTHORIZATION"]
    header.gsub(pattern, '') if header && header.match(pattern)
  end
end
