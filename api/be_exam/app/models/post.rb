class Post < ApplicationRecord
  belongs_to :user
  has_many :comments, foreign_key: :commentable_id

  validates :title, presence: true
  validates :content, presence: true

  after_create :build_slug

  def self.find_by_id_or_slug(identifier)
    from_slug = find_by(slug: identifier)
    from_slug || find(identifier)
  end

  private

  def build_slug
    self.slug = [id.to_s, title.parameterize].join('-')
    save
  end
end
