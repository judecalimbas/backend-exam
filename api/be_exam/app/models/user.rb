# frozen_string_literal: true

class User < ApplicationRecord
  has_secure_token
  has_many :posts

  attr_accessor :password_confirmation

  validates :password, presence: true, confirmation: true
  validates :password_confirmation, presence: true
  validates :name, presence: true
  validates :email, uniqueness: true, presence: true

  before_create :set_token_expiry

  def token_data
    {
      token: token,
      expires_at: expires_at,
      token_type: 'bearer'
    }
  end

  def set_token_expiry
    self.expires_at = DateTime.current + 1.day
  end
end
