# README

## System dependencies
- Ruby 2.6.3
- Node
- npm

## Setup

Clone the repository:

```sh
git clone https://gitlab.com/judecalimbas/backend-exam.git
cd backend-exam/api/be-exam
```

Install backend dependencies:

```sh
bundle install
```

Create database and run migration:

```sh
rails db:create

rails db:migrate
```

Start the app in port 8000:

```sh
rails s -p 8000
```
